#!/bin/sh
. "/usr/share/libubox/jshn.sh"

check_usbstorage_not_mounted() {
  child_device_id=$(basename "${1}")
  ROOT_DEVICE_MAJOR_MINOR=$(stat -c '%d' /)
  ROOT_DEVICE_MAJOR=$(($ROOT_DEVICE_MAJOR_MINOR>>8))
  ROOT_DEVICE_MINOR=$(($ROOT_DEVICE_MAJOR_MINOR&255))
  ROOT_DEVICE_LINK=$(readlink "/sys/dev/block/${ROOT_DEVICE_MAJOR}:${ROOT_DEVICE_MINOR}")
  if echo "${ROOT_DEVICE_LINK}" | grep -Fqe "${child_device_id}"; then
    echo "/dev/root"
  fi
  return 0
}
# Try to find existing users of this device, either host (e.g / on USB) or
# another VM
find_existing_user() {
  basename=$(basename "${device}")
  child_devices=$(find "${device}/" -type d -name "${basename}:*")
  for child_device in ${child_devices}; do
    childInterfaceClass=$(cat "${child_device}/bInterfaceClass")
    if [ "${childInterfaceClass}" = "08" ]; then
      is_used_usbstorage=$(check_usbstorage_not_mounted "${child_device}")
      if [ ! -z "${is_used_usbstorage}" ]; then
        echo "${is_used_usbstorage}"
        return 1
      fi
    fi
  done
  return 0
}
USBDEVICES=$(find /sys/bus/usb/drivers/usb/ -type l ! -name usb*)

json_init
for device in ${USBDEVICES}; do
  [ -f "${device}/bDeviceClass" ] || continue
  deviceClass=$(cat "${device}/bDeviceClass")
  # Exclude USB hubs
  if [ "${deviceClass}" = "09" ]; then
    continue
  fi
  devaddr=$(basename "${device}")
  json_add_object "${devaddr}"
  manufacturer=$(cat "${device}/manufacturer")
  json_add_string "manufacturer" "${manufacturer}"
  product=$(cat "${device}/product")
  json_add_string "product" "${product}"

  productID=$(cat "${device}/idProduct")
  productDec=$((0x$productID))
  json_add_int "idProduct" "${productDec}"

  vendorID=$(cat "${device}/idVendor")
  vendorDec=$((0x$vendorID))
  json_add_int "idVendor" "${vendorDec}"

  if [ -f "${device}/serial" ]; then
    serial=$(cat "${device}/serial")
    json_add_string "serial" "${serial}"
  fi

  existing_user=$(find_existing_user "${device}")
  if [ ! -z "${existing_user}" ]; then
    json_add_string "existing_user" "${existing_user}"
  fi
  json_close_object

done
json_dump
