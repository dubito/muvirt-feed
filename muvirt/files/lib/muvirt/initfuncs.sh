#!/bin/sh

parse_storage_arguments() {
	numargs=$(echo -n "${1}" | awk -F ',' '{print NF}')
	storage_device=$(echo -n "${1}" | awk -F ',' '{print $1}')
	additional_args=""
	for i in $(seq 2 "${numargs}"); do
		token=$(echo -n "${1}" | awk -F ',' "{print \$${i}}")
		key=$(echo -n "${token}" | awk -F '=' '{print $1}')
		value=$(echo -n "${token}" | awk -F '=' '{print $2}')

		if [[ "${key}" = "serial" ]]; then
			additional_args=",serial=${value}"
		fi
	done
	echo "file=${storage_device}${additional_args}"
}

find_usb_arguments() {
	deviceref="${1}"
	usbpath="/sys/bus/usb/devices/${deviceref}/"
	if [ ! -d "${usbpath}" ]; then
		return
	fi
	. "${usbpath}/uevent"
	busnumber=$(expr $BUSNUM + 0)
	devnumber=$(expr $DEVNUM + 0)
	echo "usb-host,id=usb${BUSNUM}${DEVNUM},hostbus=${busnumber},hostaddr=${devnumber}"
}
