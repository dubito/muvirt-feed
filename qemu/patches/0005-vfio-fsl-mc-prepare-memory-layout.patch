From 2f7f62eea4b41a0924b181b3fce35f367d126a5c Mon Sep 17 00:00:00 2001
From: Thomas Niederprüm <dubito@online.de>
Date: Thu, 06 Feb 2020 10:41:11 +0100
Subject: vfio fsl-mc: prepare memory layout

This patch squashes the following commits from the qoriq-qemu
integration branch:

- Make KVM route setting independent of PCI device
- Increased the size of requester_id field from MemTxAttrs
- hw/arm/virt: Reserve requester-id to device-id mapping for PCI
---
 accel/kvm/kvm-all.c                    | 19 +++++-------
 accel/stubs/kvm-stub.c                 |  5 +--
 hw/arm/virt.c                          |  7 +++++
 hw/intc/arm_gicv3_its_kvm.c            |  2 +-
 hw/intc/ioapic.c                       |  2 +-
 hw/misc/ivshmem.c                      |  8 +++--
 hw/vfio/common.c                       | 10 ++++--
 hw/vfio/display.c                      |  1 +
 hw/vfio/pci.c                          | 11 +++++--
 hw/vfio/platform.c                     |  2 +-
 hw/virtio/virtio-pci.c                 | 10 ++++--
 include/exec/memattrs.h                |  2 +-
 include/hw/arm/virt.h                  |  6 ++++
 include/hw/intc/arm_gicv3_its_common.h |  2 +-
 include/hw/vfio/vfio-common.h          |  2 +-
 include/sysemu/kvm.h                   | 18 ++++++-----
 target/arm/kvm.c                       | 43 ++------------------------
 target/arm/trace-events                |  3 --
 target/i386/kvm.c                      | 17 +++++-----
 target/mips/kvm.c                      |  4 +--
 target/ppc/kvm.c                       |  4 +--
 target/s390x/kvm.c                     |  4 +--
 22 files changed, 89 insertions(+), 93 deletions(-)

diff --git a/accel/kvm/kvm-all.c b/accel/kvm/kvm-all.c
index 4880a05399..a88833ecb5 100644
--- a/accel/kvm/kvm-all.c
+++ b/accel/kvm/kvm-all.c
@@ -1207,15 +1207,11 @@ int kvm_irqchip_send_msi(KVMState *s, MSIMessage msg)
     return kvm_set_irq(s, route->kroute.gsi, 1);
 }
 
-int kvm_irqchip_add_msi_route(KVMState *s, int vector, PCIDevice *dev)
+int kvm_irqchip_add_msi_route(KVMState *s, int vector, MSIMessage msg,
+                              uint32_t devid, DeviceState *dev)
 {
     struct kvm_irq_routing_entry kroute = {};
     int virq;
-    MSIMessage msg = {0, 0};
-
-    if (pci_available && dev) {
-        msg = pci_get_msi_message(dev, vector);
-    }
 
     if (kvm_gsi_direct_mapping()) {
         return kvm_arch_msi_data_to_gsi(msg.data);
@@ -1238,14 +1234,14 @@ int kvm_irqchip_add_msi_route(KVMState *s, int vector, PCIDevice *dev)
     kroute.u.msi.data = le32_to_cpu(msg.data);
     if (pci_available && kvm_msi_devid_required()) {
         kroute.flags = KVM_MSI_VALID_DEVID;
-        kroute.u.msi.devid = pci_requester_id(dev);
+        kroute.u.msi.devid = devid;
     }
     if (kvm_arch_fixup_msi_route(&kroute, msg.address, msg.data, dev)) {
         kvm_irqchip_release_virq(s, virq);
         return -EINVAL;
     }
 
-    trace_kvm_irqchip_add_msi_route(dev ? dev->name : (char *)"N/A",
+    trace_kvm_irqchip_add_msi_route(dev ? dev->id : (char *)"N/A",
                                     vector, virq);
 
     kvm_add_routing_entry(s, &kroute);
@@ -1256,7 +1252,7 @@ int kvm_irqchip_add_msi_route(KVMState *s, int vector, PCIDevice *dev)
 }
 
 int kvm_irqchip_update_msi_route(KVMState *s, int virq, MSIMessage msg,
-                                 PCIDevice *dev)
+                                 uint32_t devid, DeviceState *dev)
 {
     struct kvm_irq_routing_entry kroute = {};
 
@@ -1276,7 +1272,7 @@ int kvm_irqchip_update_msi_route(KVMState *s, int virq, MSIMessage msg,
     kroute.u.msi.data = le32_to_cpu(msg.data);
     if (pci_available && kvm_msi_devid_required()) {
         kroute.flags = KVM_MSI_VALID_DEVID;
-        kroute.u.msi.devid = pci_requester_id(dev);
+        kroute.u.msi.devid = devid;
     }
     if (kvm_arch_fixup_msi_route(&kroute, msg.address, msg.data, dev)) {
         return -EINVAL;
@@ -1399,7 +1395,8 @@ static int kvm_irqchip_assign_irqfd(KVMState *s, int fd, int virq, bool assign)
     abort();
 }
 
-int kvm_irqchip_update_msi_route(KVMState *s, int virq, MSIMessage msg)
+int kvm_irqchip_update_msi_route(KVMState *s, int virq, MSIMessage msg,
+                                 uint32_t devid, DeviceState *dev)
 {
     return -ENOSYS;
 }
diff --git a/accel/stubs/kvm-stub.c b/accel/stubs/kvm-stub.c
index 02d5170031..ff00026d03 100644
--- a/accel/stubs/kvm-stub.c
+++ b/accel/stubs/kvm-stub.c
@@ -116,7 +116,8 @@ int kvm_memcrypt_encrypt_data(uint8_t *ptr, uint64_t len)
 }
 
 #ifndef CONFIG_USER_ONLY
-int kvm_irqchip_add_msi_route(KVMState *s, int vector, PCIDevice *dev)
+int kvm_irqchip_add_msi_route(KVMState *s, int vector, MSIMessage msg,
+                              uint32_t devid, DeviceState *dev)
 {
     return -ENOSYS;
 }
@@ -130,7 +131,7 @@ void kvm_irqchip_release_virq(KVMState *s, int virq)
 }
 
 int kvm_irqchip_update_msi_route(KVMState *s, int virq, MSIMessage msg,
-                                 PCIDevice *dev)
+                                 uint32_t devid, DeviceState *dev)
 {
     return -ENOSYS;
 }
diff --git a/hw/arm/virt.c b/hw/arm/virt.c
index f69e7eb399..d785a10393 100644
--- a/hw/arm/virt.c
+++ b/hw/arm/virt.c
@@ -170,6 +170,12 @@ static const int a15irqmap[] = {
     [VIRT_PLATFORM_BUS] = 112, /* ...to 112 + PLATFORM_BUS_NUM_IRQS -1 */
 };
 
+static const DeviceIdMap deviceidmap[] = {
+    /* Currently there is only one PCI controller and requester-id
+     * to device-id mapping is 1:1 */
+    [VIRT_PCIE] =       { 0, 0x10000 },
+};
+
 static const char *valid_cpus[] = {
     ARM_CPU_TYPE_NAME("cortex-a15"),
     ARM_CPU_TYPE_NAME("cortex-a53"),
@@ -1865,6 +1871,7 @@ static void virt_3_1_instance_init(Object *obj)
 
     vms->memmap = a15memmap;
     vms->irqmap = a15irqmap;
+    vms->deviceidmap = deviceidmap;
 }
 
 static void virt_machine_3_1_options(MachineClass *mc)
diff --git a/hw/intc/arm_gicv3_its_kvm.c b/hw/intc/arm_gicv3_its_kvm.c
index 01573abb48..97ded40793 100644
--- a/hw/intc/arm_gicv3_its_kvm.c
+++ b/hw/intc/arm_gicv3_its_kvm.c
@@ -39,7 +39,7 @@ typedef struct KVMARMITSClass {
 } KVMARMITSClass;
 
 
-static int kvm_its_send_msi(GICv3ITSState *s, uint32_t value, uint16_t devid)
+static int kvm_its_send_msi(GICv3ITSState *s, uint32_t value, uint32_t devid)
 {
     struct kvm_msi msi;
 
diff --git a/hw/intc/ioapic.c b/hw/intc/ioapic.c
index 4e529729b4..226671777b 100644
--- a/hw/intc/ioapic.c
+++ b/hw/intc/ioapic.c
@@ -190,7 +190,7 @@ static void ioapic_update_kvm_routes(IOAPICCommonState *s)
             ioapic_entry_parse(s->ioredtbl[i], &info);
             msg.address = info.addr;
             msg.data = info.data;
-            kvm_irqchip_update_msi_route(kvm_state, i, msg, NULL);
+            kvm_irqchip_update_msi_route(kvm_state, i, msg, 0, NULL);
         }
         kvm_irqchip_commit_routes(kvm_state);
     }
diff --git a/hw/misc/ivshmem.c b/hw/misc/ivshmem.c
index ecfd10a29a..5f858bd67f 100644
--- a/hw/misc/ivshmem.c
+++ b/hw/misc/ivshmem.c
@@ -324,7 +324,8 @@ static int ivshmem_vector_unmask(PCIDevice *dev, unsigned vector,
     }
     assert(!v->unmasked);
 
-    ret = kvm_irqchip_update_msi_route(kvm_state, v->virq, msg, dev);
+    ret = kvm_irqchip_update_msi_route(kvm_state, v->virq, msg,
+                                       pci_requester_id(dev), DEVICE(dev));
     if (ret < 0) {
         return ret;
     }
@@ -462,11 +463,14 @@ static void ivshmem_add_kvm_msi_virq(IVShmemState *s, int vector,
 {
     PCIDevice *pdev = PCI_DEVICE(s);
     int ret;
+    MSIMessage msg;
 
     IVSHMEM_DPRINTF("ivshmem_add_kvm_msi_virq vector:%d\n", vector);
     assert(!s->msi_vectors[vector].pdev);
 
-    ret = kvm_irqchip_add_msi_route(kvm_state, vector, pdev);
+    msg = pci_get_msi_message(pdev, vector);
+    ret = kvm_irqchip_add_msi_route(kvm_state, vector, msg,
+                                    pci_requester_id(pdev), DEVICE(pdev));
     if (ret < 0) {
         error_setg(errp, "kvm_irqchip_add_msi_route failed");
         return;
diff --git a/hw/vfio/common.c b/hw/vfio/common.c
index 7c185e5a2e..aa7cab7d6f 100644
--- a/hw/vfio/common.c
+++ b/hw/vfio/common.c
@@ -768,11 +768,15 @@ static int vfio_setup_region_sparse_mmaps(VFIORegion *region,
 }
 
 int vfio_region_setup(Object *obj, VFIODevice *vbasedev, VFIORegion *region,
-                      int index, const char *name)
+                      const MemoryRegionOps *ops, int index, const char *name)
 {
     struct vfio_region_info *info;
     int ret;
 
+    if (ops == NULL) {
+        ops = &vfio_region_ops;
+    }
+
     ret = vfio_get_region_info(vbasedev, index, &info);
     if (ret) {
         return ret;
@@ -786,8 +790,8 @@ int vfio_region_setup(Object *obj, VFIODevice *vbasedev, VFIORegion *region,
 
     if (region->size) {
         region->mem = g_new0(MemoryRegion, 1);
-        memory_region_init_io(region->mem, obj, &vfio_region_ops,
-                              region, name, region->size);
+        memory_region_init_io(region->mem, obj, ops, region, name,
+                              region->size);
 
         if (!vbasedev->no_mmap &&
             region->flags & VFIO_REGION_INFO_FLAG_MMAP) {
diff --git a/hw/vfio/display.c b/hw/vfio/display.c
index dead30e626..5aec35ff59 100644
--- a/hw/vfio/display.c
+++ b/hw/vfio/display.c
@@ -264,6 +264,7 @@ static void vfio_display_region_update(void *opaque)
         /* mmap region */
         ret = vfio_region_setup(OBJECT(vdev), &vdev->vbasedev,
                                 &dpy->region.buffer,
+                                NULL,
                                 plane.region_index,
                                 "display");
         if (ret != 0) {
diff --git a/hw/vfio/pci.c b/hw/vfio/pci.c
index 5c7bd96984..21a7a6cbd7 100644
--- a/hw/vfio/pci.c
+++ b/hw/vfio/pci.c
@@ -436,6 +436,7 @@ static void vfio_add_kvm_msi_virq(VFIOPCIDevice *vdev, VFIOMSIVector *vector,
                                   int vector_n, bool msix)
 {
     int virq;
+    MSIMessage msg;
 
     if ((msix && vdev->no_kvm_msix) || (!msix && vdev->no_kvm_msi)) {
         return;
@@ -445,7 +446,10 @@ static void vfio_add_kvm_msi_virq(VFIOPCIDevice *vdev, VFIOMSIVector *vector,
         return;
     }
 
-    virq = kvm_irqchip_add_msi_route(kvm_state, vector_n, &vdev->pdev);
+    msg = pci_get_msi_message(&vdev->pdev, vector_n);
+    virq = kvm_irqchip_add_msi_route(kvm_state, vector_n, msg,
+                                     pci_requester_id(&vdev->pdev),
+                                     DEVICE(&vdev->pdev));
     if (virq < 0) {
         event_notifier_cleanup(&vector->kvm_interrupt);
         return;
@@ -473,7 +477,8 @@ static void vfio_remove_kvm_msi_virq(VFIOMSIVector *vector)
 static void vfio_update_kvm_msi_virq(VFIOMSIVector *vector, MSIMessage msg,
                                      PCIDevice *pdev)
 {
-    kvm_irqchip_update_msi_route(kvm_state, vector->virq, msg, pdev);
+    kvm_irqchip_update_msi_route(kvm_state, vector->virq, msg,
+                                 pci_requester_id(pdev), DEVICE(pdev));
     kvm_irqchip_commit_routes(kvm_state);
 }
 
@@ -2541,7 +2546,7 @@ static void vfio_populate_device(VFIOPCIDevice *vdev, Error **errp)
         char *name = g_strdup_printf("%s BAR %d", vbasedev->name, i);
 
         ret = vfio_region_setup(OBJECT(vdev), vbasedev,
-                                &vdev->bars[i].region, i, name);
+                                &vdev->bars[i].region, NULL, i, name);
         g_free(name);
 
         if (ret) {
diff --git a/hw/vfio/platform.c b/hw/vfio/platform.c
index 398db38f14..7403041c65 100644
--- a/hw/vfio/platform.c
+++ b/hw/vfio/platform.c
@@ -482,7 +482,7 @@ static int vfio_populate_device(VFIODevice *vbasedev, Error **errp)
 
         vdev->regions[i] = g_new0(VFIORegion, 1);
         ret = vfio_region_setup(OBJECT(vdev), vbasedev,
-                                vdev->regions[i], i, name);
+                                vdev->regions[i], NULL, i, name);
         g_free(name);
         if (ret) {
             error_setg_errno(errp, -ret, "failed to get region %d info", i);
diff --git a/hw/virtio/virtio-pci.c b/hw/virtio/virtio-pci.c
index a954799267..94dbffdea0 100644
--- a/hw/virtio/virtio-pci.c
+++ b/hw/virtio/virtio-pci.c
@@ -661,7 +661,12 @@ static int kvm_virtio_pci_vq_vector_use(VirtIOPCIProxy *proxy,
     int ret;
 
     if (irqfd->users == 0) {
-        ret = kvm_irqchip_add_msi_route(kvm_state, vector, &proxy->pci_dev);
+        MSIMessage msg;
+
+        msg = pci_get_msi_message(&proxy->pci_dev, vector);
+        ret = kvm_irqchip_add_msi_route(kvm_state, vector, msg,
+                                        pci_requester_id(&proxy->pci_dev),
+                                        DEVICE(&proxy->pci_dev));
         if (ret < 0) {
             return ret;
         }
@@ -794,7 +799,8 @@ static int virtio_pci_vq_vector_unmask(VirtIOPCIProxy *proxy,
         irqfd = &proxy->vector_irqfd[vector];
         if (irqfd->msg.data != msg.data || irqfd->msg.address != msg.address) {
             ret = kvm_irqchip_update_msi_route(kvm_state, irqfd->virq, msg,
-                                               &proxy->pci_dev);
+                                               pci_requester_id(&proxy->pci_dev),
+                                               DEVICE(&proxy->pci_dev));
             if (ret < 0) {
                 return ret;
             }
diff --git a/include/exec/memattrs.h b/include/exec/memattrs.h
index d4a1642098..d139f89b6c 100644
--- a/include/exec/memattrs.h
+++ b/include/exec/memattrs.h
@@ -36,7 +36,7 @@ typedef struct MemTxAttrs {
     /* Memory access is usermode (unprivileged) */
     unsigned int user:1;
     /* Requester ID (for MSI for example) */
-    unsigned int requester_id:16;
+    unsigned int requester_id:24;
 } MemTxAttrs;
 
 /* Bus masters which don't specify any attributes will get this,
diff --git a/include/hw/arm/virt.h b/include/hw/arm/virt.h
index 4cc57a7ef6..89360ffa10 100644
--- a/include/hw/arm/virt.h
+++ b/include/hw/arm/virt.h
@@ -93,6 +93,11 @@ typedef struct MemMapEntry {
     hwaddr size;
 } MemMapEntry;
 
+typedef struct DeviceIdMap {
+    uint32_t start;
+    uint32_t num;
+} DeviceIdMap;
+
 typedef struct {
     MachineClass parent;
     bool disallow_affinity_adjustment;
@@ -118,6 +123,7 @@ typedef struct {
     struct arm_boot_info bootinfo;
     const MemMapEntry *memmap;
     const int *irqmap;
+    const DeviceIdMap *deviceidmap;
     int smp_cpus;
     void *fdt;
     int fdt_size;
diff --git a/include/hw/intc/arm_gicv3_its_common.h b/include/hw/intc/arm_gicv3_its_common.h
index fd1fe64c03..0f10fcdf71 100644
--- a/include/hw/intc/arm_gicv3_its_common.h
+++ b/include/hw/intc/arm_gicv3_its_common.h
@@ -76,7 +76,7 @@ struct GICv3ITSCommonClass {
     SysBusDeviceClass parent_class;
     /*< public >*/
 
-    int (*send_msi)(GICv3ITSState *s, uint32_t data, uint16_t devid);
+    int (*send_msi)(GICv3ITSState *s, uint32_t data, uint32_t devid);
     void (*pre_save)(GICv3ITSState *s);
     void (*post_load)(GICv3ITSState *s);
 };
diff --git a/include/hw/vfio/vfio-common.h b/include/hw/vfio/vfio-common.h
index 1b434d02f6..04eed6cdc2 100644
--- a/include/hw/vfio/vfio-common.h
+++ b/include/hw/vfio/vfio-common.h
@@ -168,7 +168,7 @@ void vfio_region_write(void *opaque, hwaddr addr,
 uint64_t vfio_region_read(void *opaque,
                           hwaddr addr, unsigned size);
 int vfio_region_setup(Object *obj, VFIODevice *vbasedev, VFIORegion *region,
-                      int index, const char *name);
+                      const MemoryRegionOps *ops, int index, const char *name);
 int vfio_region_mmap(VFIORegion *region);
 void vfio_region_mmaps_set_enabled(VFIORegion *region, bool enabled);
 void vfio_region_exit(VFIORegion *region);
diff --git a/include/sysemu/kvm.h b/include/sysemu/kvm.h
index 97d8d9d0d5..b8e4fe0fbd 100644
--- a/include/sysemu/kvm.h
+++ b/include/sysemu/kvm.h
@@ -385,11 +385,11 @@ void kvm_arch_on_sigbus_vcpu(CPUState *cpu, int code, void *addr);
 void kvm_arch_init_irq_routing(KVMState *s);
 
 int kvm_arch_fixup_msi_route(struct kvm_irq_routing_entry *route,
-                             uint64_t address, uint32_t data, PCIDevice *dev);
+                             uint64_t address, uint32_t data, DeviceState *dev);
 
 /* Notify arch about newly added MSI routes */
 int kvm_arch_add_msi_route_post(struct kvm_irq_routing_entry *route,
-                                int vector, PCIDevice *dev);
+                                int vector, DeviceState *dev);
 /* Notify arch about released MSI routes */
 int kvm_arch_release_virq_post(int virq);
 
@@ -486,16 +486,18 @@ void kvm_init_cpu_signals(CPUState *cpu);
  * kvm_irqchip_add_msi_route - Add MSI route for specific vector
  * @s:      KVM state
  * @vector: which vector to add. This can be either MSI/MSIX
- *          vector. The function will automatically detect whether
- *          MSI/MSIX is enabled, and fetch corresponding MSI
- *          message.
+ *          vector.
+ * @msg:    MSI message
+ * @devid:  stream ID. It can be equal with PCI requester ID for some
+ *          platforms.
  * @dev:    Owner PCI device to add the route. If @dev is specified
- *          as @NULL, an empty MSI message will be inited.
+ *          as @NULL, an empty MSI message will be passed.
  * @return: virq (>=0) when success, errno (<0) when failed.
  */
-int kvm_irqchip_add_msi_route(KVMState *s, int vector, PCIDevice *dev);
+int kvm_irqchip_add_msi_route(KVMState *s, int vector, MSIMessage msg,
+                              uint32_t devid, DeviceState *dev);
 int kvm_irqchip_update_msi_route(KVMState *s, int virq, MSIMessage msg,
-                                 PCIDevice *dev);
+                                 uint32_t devid, DeviceState *dev);
 void kvm_irqchip_commit_routes(KVMState *s);
 void kvm_irqchip_release_virq(KVMState *s, int virq);
 
diff --git a/target/arm/kvm.c b/target/arm/kvm.c
index 44dd0ce6ce..f4b2ecf23b 100644
--- a/target/arm/kvm.c
+++ b/target/arm/kvm.c
@@ -20,10 +20,8 @@
 #include "sysemu/kvm.h"
 #include "kvm_arm.h"
 #include "cpu.h"
-#include "trace.h"
 #include "internals.h"
 #include "hw/arm/arm.h"
-#include "hw/pci/pci.h"
 #include "exec/memattrs.h"
 #include "exec/address-spaces.h"
 #include "hw/boards.h"
@@ -719,48 +717,13 @@ int kvm_arm_vgic_probe(void)
 }
 
 int kvm_arch_fixup_msi_route(struct kvm_irq_routing_entry *route,
-                             uint64_t address, uint32_t data, PCIDevice *dev)
+                             uint64_t address, uint32_t data, DeviceState *dev)
 {
-    AddressSpace *as = pci_device_iommu_address_space(dev);
-    hwaddr xlat, len, doorbell_gpa;
-    MemoryRegionSection mrs;
-    MemoryRegion *mr;
-    int ret = 1;
-
-    if (as == &address_space_memory) {
-        return 0;
-    }
-
-    /* MSI doorbell address is translated by an IOMMU */
-
-    rcu_read_lock();
-    mr = address_space_translate(as, address, &xlat, &len, true,
-                                 MEMTXATTRS_UNSPECIFIED);
-    if (!mr) {
-        goto unlock;
-    }
-    mrs = memory_region_find(mr, xlat, 1);
-    if (!mrs.mr) {
-        goto unlock;
-    }
-
-    doorbell_gpa = mrs.offset_within_address_space;
-    memory_region_unref(mrs.mr);
-
-    route->u.msi.address_lo = doorbell_gpa;
-    route->u.msi.address_hi = doorbell_gpa >> 32;
-
-    trace_kvm_arm_fixup_msi_route(address, doorbell_gpa);
-
-    ret = 0;
-
-unlock:
-    rcu_read_unlock();
-    return ret;
+    return 0;
 }
 
 int kvm_arch_add_msi_route_post(struct kvm_irq_routing_entry *route,
-                                int vector, PCIDevice *dev)
+                                int vector, DeviceState *dev)
 {
     return 0;
 }
diff --git a/target/arm/trace-events b/target/arm/trace-events
index 6b759f9d4f..9e37131115 100644
--- a/target/arm/trace-events
+++ b/target/arm/trace-events
@@ -8,6 +8,3 @@ arm_gt_tval_write(int timer, uint64_t value) "gt_tval_write: timer %d value 0x%"
 arm_gt_ctl_write(int timer, uint64_t value) "gt_ctl_write: timer %d value 0x%" PRIx64
 arm_gt_imask_toggle(int timer, int irqstate) "gt_ctl_write: timer %d IMASK toggle, new irqstate %d"
 arm_gt_cntvoff_write(uint64_t value) "gt_cntvoff_write: value 0x%" PRIx64
-
-# target/arm/kvm.c
-kvm_arm_fixup_msi_route(uint64_t iova, uint64_t gpa) "MSI iova = 0x%"PRIx64" is translated into 0x%"PRIx64
diff --git a/target/i386/kvm.c b/target/i386/kvm.c
index b2401d13ea..cf74f4b4c6 100644
--- a/target/i386/kvm.c
+++ b/target/i386/kvm.c
@@ -3663,7 +3663,8 @@ void kvm_arch_init_irq_routing(KVMState *s)
         /* If the ioapic is in QEMU and the lapics are in KVM, reserve
            MSI routes for signaling interrupts to the local apics. */
         for (i = 0; i < IOAPIC_NUM_PINS; i++) {
-            if (kvm_irqchip_add_msi_route(s, 0, NULL) < 0) {
+            MSIMessage msg = {0, 0};
+            if (kvm_irqchip_add_msi_route(s, 0, msg, 0,  NULL) < 0) {
                 error_report("Could not enable split IRQ mode.");
                 exit(1);
             }
@@ -3831,7 +3832,7 @@ int kvm_device_msix_deassign(KVMState *s, uint32_t dev_id)
 }
 
 int kvm_arch_fixup_msi_route(struct kvm_irq_routing_entry *route,
-                             uint64_t address, uint32_t data, PCIDevice *dev)
+                             uint64_t address, uint32_t data, DeviceState *dev)
 {
     X86IOMMUState *iommu = x86_iommu_get_default();
 
@@ -3850,7 +3851,7 @@ int kvm_arch_fixup_msi_route(struct kvm_irq_routing_entry *route,
         src.data = route->u.msi.data;
 
         ret = class->int_remap(iommu, &src, &dst, dev ? \
-                               pci_requester_id(dev) : \
+                               pci_requester_id(PCI_DEVICE(dev)) : \
                                X86_IOMMU_SID_INVALID);
         if (ret) {
             trace_kvm_x86_fixup_msi_error(route->gsi);
@@ -3893,15 +3894,17 @@ static void kvm_update_msi_routes_all(void *private, bool global,
         if (!msix_enabled(dev) && !msi_enabled(dev)) {
             continue;
         }
-        msg = pci_get_msi_message(dev, entry->vector);
-        kvm_irqchip_update_msi_route(kvm_state, entry->virq, msg, dev);
+        msg = pci_get_msi_message(entry->dev, entry->vector);
+        kvm_irqchip_update_msi_route(kvm_state, entry->virq,
+                                     msg, pci_requester_id(entry->dev),
+                                     DEVICE(entry->dev));
     }
     kvm_irqchip_commit_routes(kvm_state);
     trace_kvm_x86_update_msi_routes(cnt);
 }
 
 int kvm_arch_add_msi_route_post(struct kvm_irq_routing_entry *route,
-                                int vector, PCIDevice *dev)
+                                int vector, DeviceState *dev)
 {
     static bool notify_list_inited = false;
     MSIRouteEntry *entry;
@@ -3914,7 +3917,7 @@ int kvm_arch_add_msi_route_post(struct kvm_irq_routing_entry *route,
     }
 
     entry = g_new0(MSIRouteEntry, 1);
-    entry->dev = dev;
+    entry->dev = PCI_DEVICE(dev);
     entry->vector = vector;
     entry->virq = route->gsi;
     QLIST_INSERT_HEAD(&msi_route_list, entry, list);
diff --git a/target/mips/kvm.c b/target/mips/kvm.c
index 8e72850962..0cb39e32a5 100644
--- a/target/mips/kvm.c
+++ b/target/mips/kvm.c
@@ -1032,13 +1032,13 @@ int kvm_arch_get_registers(CPUState *cs)
 }
 
 int kvm_arch_fixup_msi_route(struct kvm_irq_routing_entry *route,
-                             uint64_t address, uint32_t data, PCIDevice *dev)
+                             uint64_t address, uint32_t data, DeviceState *dev)
 {
     return 0;
 }
 
 int kvm_arch_add_msi_route_post(struct kvm_irq_routing_entry *route,
-                                int vector, PCIDevice *dev)
+                                int vector, DeviceState *dev)
 {
     return 0;
 }
diff --git a/target/ppc/kvm.c b/target/ppc/kvm.c
index f81327d6cd..623ed10e83 100644
--- a/target/ppc/kvm.c
+++ b/target/ppc/kvm.c
@@ -2687,13 +2687,13 @@ void kvmppc_write_hpte(hwaddr ptex, uint64_t pte0, uint64_t pte1)
 }
 
 int kvm_arch_fixup_msi_route(struct kvm_irq_routing_entry *route,
-                             uint64_t address, uint32_t data, PCIDevice *dev)
+                             uint64_t address, uint32_t data, DeviceState *dev)
 {
     return 0;
 }
 
 int kvm_arch_add_msi_route_post(struct kvm_irq_routing_entry *route,
-                                int vector, PCIDevice *dev)
+                                int vector, DeviceState *dev)
 {
     return 0;
 }
diff --git a/target/s390x/kvm.c b/target/s390x/kvm.c
index 2ebf26adfe..247183541c 100644
--- a/target/s390x/kvm.c
+++ b/target/s390x/kvm.c
@@ -1993,7 +1993,7 @@ int kvm_s390_vcpu_interrupt_post_load(S390CPU *cpu)
 }
 
 int kvm_arch_fixup_msi_route(struct kvm_irq_routing_entry *route,
-                             uint64_t address, uint32_t data, PCIDevice *dev)
+                             uint64_t address, uint32_t data, DeviceState *dev)
 {
     S390PCIBusDevice *pbdev;
     uint32_t vec = data & ZPCI_MSI_VEC_MASK;
@@ -2020,7 +2020,7 @@ int kvm_arch_fixup_msi_route(struct kvm_irq_routing_entry *route,
 }
 
 int kvm_arch_add_msi_route_post(struct kvm_irq_routing_entry *route,
-                                int vector, PCIDevice *dev)
+                                int vector, DeviceState *dev)
 {
     return 0;
 }
-- 
2.25.0

