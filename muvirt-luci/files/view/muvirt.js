'use strict';
'require ui';
'require rpc';
'require uci';
'require form';
'require network';
'require view.muvirtwidgets';

var callSpawnConsole = rpc.declare({
  object: 'muvirt',
  method: 'console',
  params: [ 'vmname' ],
});

var callStartVM = rpc.declare({
  object: 'muvirt',
  method: 'start',
  params: ['vmname'],
});

var callStopVM = rpc.declare({
  object: 'muvirt',
  method: 'stop',
  params: ['vmname'],
});

function generateRandomBaseMAC() {
  var mac = "52:54:00"; // QEMU OUI
  for (var i=0; i<3; i++) {
    var nibble = Math.round(Math.random() * 255).toString(16);
    if (nibble.length == 1) {
      nibble = "0"+nibble;
    }
    mac = mac + ":"+nibble;
  }
  return mac;
}
function handleSpawnConsoleResponse(resp) {
  open(location.protocol+"//"+resp.username+':'+resp.password+"@"+location.host+"/vmconsole/"+resp.vmname+"/"+resp.sessionid+"/");
}

function spawn_webconsole(vmname) {
  return callSpawnConsole(vmname).then(handleSpawnConsoleResponse);
}
function handle_vmactionbutton(event) {
  var target = event.target;
  var vmname = target.getAttribute("data-vmname");
  var action = target.getAttribute("data-action");
  if ("stop" == action) {
    callStopVM(vmname);
    target.innerText = _('Stopping');
    target.disabled = true;
  } else if ("start" == action) {
    callStartVM(vmname);
    target.innerText = _('Starting');
    target.disabled = true;
  }
}
return L.view.extend({
  renderOverviewRowActions: function(section_id) {
    console.log("renderRowActions for"+section_id);
    var tdEl = this.super('renderRowActions', [ section_id, _('Edit') ]);

    var vmrunning = (section_id in this.vmstatus && this.vmstatus[section_id]["running"] == true);

    var stopStartButton = E('button',
      {'type': 'button', 'class':'cbi-button', 'data-vmname':section_id},
      _('Start'));

    if (vmrunning) {
      stopStartButton.innerText = _('Stop');
      stopStartButton.class = 'cbi-button-negative';
      stopStartButton.setAttribute("data-action", "stop");
    } else {
      stopStartButton.setAttribute("data-action", "start");
    }
    stopStartButton.onclick = handle_vmactionbutton;

    var consoleButton = E('button', {'type': 'button', 'class':'cbi-button'},  _('Console'));
    if (vmrunning) {
      consoleButton.onclick = function(event) {
        spawn_webconsole(section_id);
      };
    } else {
      consoleButton.disabled = true;
    }

    L.dom.content(tdEl.lastChild, [
      stopStartButton,
      consoleButton,
      tdEl.lastChild.firstChild,
      tdEl.lastChild.lastChild
    ]);
    return tdEl;
  },
  doRender: function() {
    var m,s,o;
    m = new form.Map('virt', _('Virtualization'));
    m.tabbed = true;

    var vmstatus = this.vmstatus;

    s = m.section(form.GridSection, 'vm', _('Virtual Machines'));
    s.addremove = true;
    s.anonymous = false;
    s.addbtntitle = _('Add new virtual machine');
    s.vmstatus = this.vmstatus;
    s.renderRowActions = this.renderOverviewRowActions;

    s.tab('general', _('General'))
    s.tab('network', _('Network'))
    s.tab('storage', _('Storage'))
    s.tab('passthrough', _('Passthrough'))
    s.tab('provisioning', _('Provisioning'))
    s.tab('misc', _('Miscellaneous'))

    s.filter = function(section_id) {
      console.log("filter for:" +section_id);
      return true;
    };



    s.modaltitle = function(section_id) {
      return _('Virtual Machines') + ' » ' + section_id.toUpperCase();
    }


     o = s.option(form.Flag, 'enable', _('Enabled'));
     o.modalonly = false;

     o = s.option(form.Flag, 'provisioned', _('Provisioned'));
     o.modalonly = false;

     o = s.option(form.Flag, '_running', _('Running'));
     o.cfgvalue = function(s) {
       console.log("_running textvalue for: "+s);
       if (s in vmstatus) {
         return vmstatus[s]["running"];
       }
       return _false;
     };
     o.modalonly = false;

     o = s.option(form.DummyValue, '_resource', _('Resources'));
     o.modalonly = false;
     o.textvalue = this.renderResourceStatusCell;

     o = s.option(form.DummyValue, '_netstat', _('Network'));
     o.modalonly = false;
     o.textvalue = this.renderNetworkStatusCell.bind(this);


     /* Modal dialog only options */
     o = s.taboption('general', form.Value, 'numprocs', _('Number of vCPUs'));
     o.default = '1';
     o.modalonly = true;
     o.datatype = 'uinteger';

     o = s.taboption('general', form.Value, 'memory', _('RAM'));
     o.datatype = 'uinteger';
     o.default = '512';
     o.modalonly = true;

     o = s.taboption('general', form.Flag,  'ballooning', _('Enable memory ballooning'),
       _('Allows the host to request memory back from a virtual machine'));
     o.modalonly = true;

     o = s.taboption('general', form.Flag, 'enable', _('Enable this VM'),
       _('If unchecked, VM will not be started on boot'));
    o.modalonly = true;

     o = s.taboption('general', form.Flag, 'provisioned', _('Is this VM provisioned?'),
       _('If unchecked, the specified image (see Provisioning tab) will be downloaded on first use'));
    o.modalonly = true;


     o = s.taboption('network', form.Value, 'mac', _('Base MAC address'),
     _('Applied to the first interface and incremented thereafter. MACs can be overriden individually below'));
     o.modalonly = true;
     o.default = generateRandomBaseMAC();

     o = s.taboption('network', view_muvirtwidgets.OrderedNetworkSelect, 'network', _('Networks to connect to'))
     o.multiple = true;
     o.bridgeonly = true;
     //o.noaliases = false;
     o.filter = function(section_id, value) {
       console.log("Network select: "+section_id + " value: "+value);
       return true;
     };
     o.modalonly = true;

     o = s.taboption('storage', view_muvirtwidgets.DiskSelect, 'disks', _('Storage devices'));
     o.modalonly = true;

     o = s.taboption('passthrough', view_muvirtwidgets.USBPassthroughSelect, 'usbdevice', _('USB Devices to passthrough'));
     o.modalonly = true;

     o = s.taboption('provisioning', form.Value, 'imageurl', _('Image URL'),
        _('Image to download'));
     o.modalonly = true;

     o = s.taboption('provisioning', form.Value, 'imagesum256', _('Image SHA256SUM'));
     o.modalonly = true;

     o = s.taboption('provisioning', form.Value, 'disksize', _('Disk size'),
        _('In megabytes (MB). Image will be resized to this during provisioning'));
     o.datatype = 'uinteger';
     o.modalonly = true;

     o = s.taboption('misc', form.Value, 'shutdowntimeout', _('Graceful shutdown timeout'),
        _('Default 60 seconds'));
     o.modalonly = true;

     o = s.taboption('misc', form.Value, 'bios', _('EFI binary override'),
      _('Default is /lib/muvirt/QEMU_EFI.fd'));
     o.modalonly = true;

     /* End tab - start global options */
     s = m.section(form.TypedSection, 'muvirt', _('Global options'))
     s.addremove = false;
     s.anonymous = false;

     o = s.option(form.Value, 'hugetlb', _('HugeTLB area size (in MB)'), _("Reserve an area of memory for better virtual machine performance"));
     o.datatype = 'uinteger';

     o = s.option(form.Value, 'scratch', _('Scratch area path'), _("Scratch folder to download virtual machine images for provisioning"));

    return m.render();
  },
  render: function() {
    var instanceinfo = L.Request.get(L.url('admin/muvirt/muvirt_instances'));

    return Promise.resolve(instanceinfo).
      then(this.processStatusResponse.bind(this)).then(this.doRender.bind(this));
  },
  processStatusResponse: function(response) {
    console.log("processsStatusResponse");
    this.vmstatus = response.json();
  },
  renderNetworkStatusCell: function(vmname) {
    console.log("renderVMStatusCell: "+vmname);
    if (!(vmname in this.vmstatus))
      return;

    var networktext = "";

    if ("network" in this.vmstatus[vmname]) {
        networktext = this.vmstatus[vmname]["network"];
        networktext = networktext.replace("\n","<br/>");
      }

    var node = E('div', {
      'id': '%s-ifc-description'.format(vmname)
    }, networktext);
    return node;
  },
  renderResourceStatusCell: function(vmname) {
    /* TODO: This is bad */
    var vmconfig = this.section.map.data.state.values["virt"][vmname];

    var resourcetext = vmconfig["numprocs"] + " vCPU<br/>" +
      vmconfig["memory"]+"M";

    var node = E('div', {
      'id': '%s-ifc-description'.format(vmname)
    }, resourcetext);
    return node;
  },

	handleSaveApply: null,
	handleReset: null
});
