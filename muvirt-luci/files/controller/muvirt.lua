--[[

LuCI module for muvirt

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Author: Mathew McBride <matt@traverse.com.au>

]]--

module("luci.controller.muvirt", package.seeall)

local uci  = require "luci.model.uci".cursor()
local util = require "luci.util"
local nx   = require "nixio"

function index()
	if not nixio.fs.access("/etc/config/virt") then
		return
	end

	page = node("admin", "muvirt")
	page.target = view("muvirt")
	page.title = _("Virtualization")
	page.order = 70

	entry({"admin", "muvirt", "muvirt_instances"}, call("muvirt_get_instances"), nil).leaf = true
end

function muvirt_get_instances()
	local knownvms = {}
	local instances = util.ubus("service","list", {name = "muvirt"})
	local configs = uci:foreach("virt","vm", function(s)
		local name = s[".name"]
		properties = {}
		properties["enabled"] = (s.enable == "1" and true or false)
		properties["provisioned"] = (s.provisioned == "1" and true or false)
		properties["numprocs"] = (s.numprocs and tonumber(s.numprocs) or 1)
		properties["mem"] = (s.memory and tonumber(s.memory) or 0)
		properties["running"] = false
		knownvms[name] = properties
	end
	)
	if instances.muvirt ~= nil and instances.muvirt.instances ~= nil then
		allinstances = instances["muvirt"]["instances"]
		for vmname, vmprops in pairs(allinstances) do
			running = vmprops["running"]
			-- Make sure the VM is in the current UCI state - if deleted but
			-- previously running it might not be.
			if knownvms[vmname] ~= nil then
				knownvms[vmname]["running"] = running
				if running == true then
					local net_info_pipe = io.popen("/lib/muvirt/muvirt-guest-info-web %s" % luci.util.shellquote(vmname))
					local net_info = net_info_pipe:read("*a")
					knownvms[vmname]["network"] = net_info
					net_info_pipe:close()
				end
			end
		end
	end
  luci.http.prepare_content("application/json")
  luci.http.write_json(knownvms)
end
